<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Mockery\Exception;
use App\Log ;
use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\Filter;


class LogController extends Controller
{
    public function index(Request $request){
        $logs = QueryBuilder::for(Log::class)
            ->allowedFilters(Filter::exact('response_code'))
            ->allowedSorts('id')
            ->get();
        if(!count($logs)){
            return response()->json(['msg' => "No Logs ! ."] , 404);
        }
        return response()->json(['msg' => '' , "data" => $logs] , 200);
//        $logs = Log::with('user')->get();
//        if(!count($logs)){
//            return response()->json(['msg' => "No Logs ! ."] , 404);
//        }
//        return response()->json(['msg' => '' , "data" => $logs] , 200);
    }
}
