<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use \GuzzleHttp\Client ;
use Guzzle\Http\Exception\ClientErrorResponseException;
use Mockery\Exception;
use App\Log ;

class MassengerController extends Controller
{
    /**
     * Create a Massenger controller instance.
     *
     * @return void
     */
    private $client ;
    private $facebookGraphUrl ;
    public $request ;
    public function __construct(Request $request)
    {
        $this->client = new Client();
        $this->facebookGraphUrl = 'https://graph.facebook.com/v2.6/me/messages?access_token=' . env("PAGE_ACCESS_TOKEN") ;
        $this->request = $request ;
    }

    /** ForTesting with user interact */
    public function receive()
    {
        //return response()->json('Thank you , But there is no UI there :) .');
        /** Validate Post request when user interact from Facebook */
        $this->validate($this->request, [
            'entry' => 'required'
        ]);



        $data = $this->request->all();
        //get the user’s id
        $fbUserId = $data["entry"][0]["messaging"][0]["sender"]["id"];

        $this->request['fbUser'] = $fbUserId ;
        $this->request['sendBy'] = 'id' ;

        $this->sendMsgByID($fbUserId , "أهلا بك في تطبيق دليل الجوال");

    }

    public function sendMsgByID($fbUserId , $msgText)
    {
        $recipient =  [
            "id" => $fbUserId
        ];

        return $this->sendToMassenger($recipient , $msgText);
    }

    public function sendByMobile($fbUserMobileNumber , $msgText)
    {
        $recipient =  [
            "phone_number" => $fbUserMobileNumber
        ];

        return $this->sendToMassenger($recipient, $msgText);
    }

    public function sendToMassenger($recipient , $msgText = 'Welcome To Our App .'){

        $log_data = [] ;
        /** @var
         * $messageData Create Facebook message structure */
        $messageData = [
            "recipient" => $recipient ,
            "message" => [
                "text" => $msgText
            ],
            "messaging_type" => "MESSAGE_TAG",
            "tag" =>  "PAYMENT_UPDATE"
        ];

        try{
            $response = $this->client->request('POST', $this->facebookGraphUrl, [
                'json' => $messageData
            ]);
            /** Save request */
            $log_data['msg'] = json_encode($messageData['message']) ;
            $log_data['recipient'] = $this->request->fbUser ;
            $log_data['sent_by'] = $this->request->sendBy ;
            $log_data['full_encoded_request'] = json_encode($messageData);
            $log_data['response_code'] = $response->getStatusCode() ;
            $log_data['full_encoded_response'] = json_encode($response);
            $this->saveDbLog($log_data);
            if($response->getStatusCode() === 200){
                return response()->json(['msg' => "Message has been sent"] , 200) ;
            }
        }catch(\Exception  $e) {
            /** Save request */
            $log_data['msg'] = json_encode($messageData['message']) ;
            $log_data['recipient'] = $this->request->fbUser ;
            $log_data['sent_by'] = $this->request->sendBy ;
            $log_data['full_encoded_request'] = json_encode($messageData);
            $log_data['response_code'] = $e->getCode() ;
            $log_data['full_encoded_response'] = $e->getMessage();
            $this->saveDbLog($log_data);
            return response()->json(['msg' => $e->getMessage()] , 400) ;
        }
    }

    public function saveDbLog($data){

        try{
            Log::create([
                "ip" => $this->request->ip() ,
                "recipient" => $data['recipient'] ,
                'sent_by' => $data['sent_by'] ,
                "msg" => $data['msg'],
                "full_encoded_request" => $data['full_encoded_request'] ,
                "user_id" => $this->request->user()->id ,
                "response_code" => $data['response_code'] ,
                "full_encoded_response" => $data['full_encoded_response']
            ]);


        }catch(\Exception $exception){
            return $exception->getMessage();
        }

    }
    
    public function sendFbMsg(){
        /** Validate Post request when user interact from Facebook */
        $this->validate($this->request, [
            'fbUser' => 'required',
            'sendBy' => 'required|in:id,mobile',
            'msg' => 'required'
        ]);

        switch ($this->request->sendBy) {
            case "id":
                return $this->sendMsgByID($this->request->fbUser , $this->request->msg);
                break;
            case "mobile":
                return $this->sendByMobile($this->request->fbUser , $this->request->msg);
                break;
            default :
                return response()->json(['msg' => "bad Request"] , 400) ;
                break ;
        }

    }



    //
}
