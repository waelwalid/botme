<?php
/**
 * Created by PhpStorm.
 * User: TheSpecialOne-
 * Date: 1/8/2019
 * Time: 10:33 AM
 */

namespace App;
use Illuminate\Database\Eloquent\Model;


class Log extends Model
{
    protected $fillable = [
        'ip' ,
        'client_mobile_number' ,
        'response_code' ,
        "msg" ,
        "full_encoded_request",
        'full_encoded_response',
        'user_id' ,
        'recipient',
        'sent_by',
        'created_at' ,
        'updated_at'
    ] ;

    public function user(){
        return $this->belongsTo(\App\User::class);
    }
}