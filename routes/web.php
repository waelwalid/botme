<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/** Facebook verfication middleware */
$router->group(['middleware' => 'facebookVerify'], function () use ($router) {
    $router->get('/', "MassengerController@receive");
});
$router->post('/', "MassengerController@receive");


$router->group(['prefix' => "v1"] , function () use ($router){

    /** Generate user token */
    $router->post('auth/login', ['uses' => 'AuthController@authenticate']);

    /** Auth Routes */
    $router->group(['middleware' => 'auth'], function() use ($router) {
//        /** send msg to facebook massenger */
//        $router->post('/sendByMobile', ['uses' =>  "MassengerController@sendByMobile"]);

//        /** send msg to facebook massenger */
//        $router->post('/sendById', ['uses' =>  "MassengerController@sendMsgByID"]);
        /** send activation code msg to facebook massenger */
        $router->post('/sendFbMsg', ['uses' =>  "MassengerController@sendFbMsg"]);
        /** Revoke access token */
        $router->get('auth/revoke', ['uses' => 'AuthController@revoke']);
        /** Get Logs */
        $router->get('/log', ['uses' =>  "LogController@index"]);

        }
    );
});







$router->get("/sendtomobile/{mobileNumber}" , "MassengerController@sendTextMessageByMobile");
